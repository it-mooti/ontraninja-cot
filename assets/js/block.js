var el = wp.element.createElement;
const {registerBlockType} = wp.blocks; //Blocks API
const {createElement} = wp.element; //React.createElement
const {__} = wp.i18n; //translation functions
wp.blocks.registerBlockType('ontraninja-cot1/cot-block', {

    title: 'Ontraninja COT', // Block name visible to user

    icon: 'lightbulb', // Toolbar icon can be either using WP Dashicons or custom SVG

    category: 'common', // Under which category the block would appear

    attributes: {
        object_id: { type: 'string', default: 'default' },
        fields_label: { type: 'string' },
        fields_key: { type: 'string' },
        sort_by: { type: 'string' },
        sort: { type: 'string' }
    },

    edit: function(props) {
        function updateObjectID( event ) {
            props.setAttributes( { object_id: event.target.value } );
        }
        function updateFieldsLabel( event ) {
            props.setAttributes( { fields_label: event.target.value } );
        }
        function updateFieldsKey( event ) {
            props.setAttributes( { fields_key: event.target.value } );
        }
        function updateSortBy( event ) {
            props.setAttributes( { sort_by: event.target.value } );
        }
        function updateSort( event ) {
            props.setAttributes( { sort: event.target.value } );
        }
        return el( 'div',
            {
                className: 'ontraninja-cot1'
            },
            el(
                'input',
                {
                    type: 'text',
                    placeholder: 'Enter Object ID',
                    value: props.attributes.object_id,
                    onChange: updateObjectID,
                    style: { width: '100%' }
                }
            ),
            el(
                'textarea',
                {
                    type: 'textarea',
                    placeholder: 'Enter Fields Label',
                    value: props.attributes.fields_label,
                    onChange: updateFieldsLabel,
                    style: { width: '100%' }
                }
            ),
            el(
                'textarea',
                {
                    type: 'textarea',
                    placeholder: 'Enter Fields Key',
                    value: props.attributes.fields_key,
                    onChange: updateFieldsKey,
                    style: { width: '100%' }
                }
            ),
            el(
                'textarea',
                {
                    type: 'textarea',
                    placeholder: 'Enter Sort By',
                    value: props.attributes.sort_by,
                    onChange: updateSortBy,
                    style: { width: '100%' }
                }
            ),
            el(
                'select',
                {
                    onChange: updateSort,
                    value: props.attributes.sort,
                },
                el("option", {value: "asc" }, "Ascending"),
                el("option", {value: "desc" }, "Descending")
            ),
            el( 'div', {}, 'waqar' )
        ); // End return

    },

    save: function(props) {
        return; null
    }
});