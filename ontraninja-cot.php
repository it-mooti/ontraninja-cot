<?php
/*
 * Plugin Name: Custom Objects Tables for PilotPress
 * Author: ITMOOTI
 * Author URL: https://itmooti.com/
 * Version: 1.1
 * */
require_once('ontraport.class.php');
class ontraninja_cot{
    public function __construct(){
        add_action('init',array( $this, 'init'));
    }
    public function init(){
        //add_action( 'enqueue_block_editor_assets', array( $this, 'block_editor_assets' ) );
        add_shortcode('ontraninja_cot', array($this, 'ontraninja_cot'));
    }
    public function block_editor_assets(){
        return;
        wp_enqueue_script(
            'ontraninja-cot1-block-editor',
            plugins_url( 'assets/js/block.js', __FILE__ ),
            array( 'wp-blocks', 'wp-element' )
        );
        register_block_type('ontraninja-cot1/cot-block', array(
            'editor_script' => 'ontraninja-cot1-block-editor',
            'render_callback' => array( $this, 'ontraninja_cot' ),
            'attributes' => [
               'object_id' => [
                    'default' => ''
                ],
                'fields_label' => [
                    'default' => ''
                ],
                'fields_key' => [
                    'default' => ''
                ],
                'sort_by' => [
                    'default' => ''
                ],
                'sort' => [
                    'default' => ''
                ]
            ]
        ));

        $ontraninja_cot_settings = get_transient('ontraninja_cot_settings');
        if( is_null($ontraninja_cot_settings) || !$ontraninja_cot_settings ){
            global $pilotpress;
            if($pilotpress->get_setting("api_key") && $pilotpress->get_setting("app_id")) {
                $op = new OntraNinjaOP($pilotpress->get_setting("app_id"), $pilotpress->get_setting("api_key"));
                var_dump($op->getObjectFields( 38 ));
                die;
                $objects = array(
                    '38' => array(
                        'name' => 'Commissions',
                        'fields' => $op->getObjectFields( 38 )
                    ),
                    '52' => array(
                        'name' => 'Orders',
                        'fields' => $op->getObjectFields( 52 )
                    ),
                    '17' => array(
                        'name' => 'Purchases',
                        'fields' => $op->getObjectFields( 17 )
                    ),
                );
                $custom_objects = $op->searchObject(99);
                if( is_array( $custom_objects ) && count( $custom_objects ) > 0 ) {
                    foreach( $custom_objects as $custom_object ){
                        $objects[ $custom_object[ 'id' ] ] = array(
                            'name' => $custom_object[ 'name' ],
                            'fields' => $op->getObjectFields( $custom_object[ 'id' ] )
                        );
                    }
                }
                $ontraninja_cot_settings['objects'] = $objects;
                set_transient('ontraninja_cot_settings', $ontraninja_cot_settings);
            }
        }
        wp_localize_script( 'ontraninja-cot1-block-editor', 'ontraninja_cot',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'settings' => $ontraninja_cot_settings ) );
        wp_enqueue_style(
            'ontraninja-cot1-block-editor',
            plugins_url( 'assets/css/block.css', __FILE__ ),
            array()
        );
    }
    public function ontraninja_cot($attr, $content){
        global $pilotpress;
        $contact_id = $pilotpress->get_setting('contact_id', 'user');
        if( !empty( $contact_id ) && $pilotpress->get_setting("api_key") && $pilotpress->get_setting("app_id")) {
            $is_add = isset($attr["add_form_id"]);
            $is_edit = isset($attr["edit_form_id"]);
            if($is_edit){ wp_enqueue_style( ‘dashicons’ );}
            $fields_label = explode(",", $attr['fields_label']);
            $colspan = count($fields_label) + ($is_edit?1:0);

            ob_start();
            $op = new OntraNinjaOP($pilotpress->get_setting("app_id"), $pilotpress->get_setting("api_key"));
            ?>
            <div class="table-responsive">
                <?php
                if($is_add){
                    ?>
                    <a href="#" class="ontraninja-add-new-btn"><span class="dashicons dashicons-plus"></span> Create</a>
                    <?php
                }
                ?>
                <table class="table">
                    <thead>

                        <tr>
                            <?php
                            if (!empty($attr['fields_label'])) {

                                $cnt=1;
                                foreach ($fields_label as $label) {
                                    ?>
                                    <?php echo $cnt==1&&$is_edit?'<th>&nbsp;</th>':'';?>
                                    <th class="op_field_label_<?php echo sanitize_title(trim($label)) ?>"><?php echo $label ?></th>
                                    <?php
                                    $cnt++;
                                }
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $shipments = array();
                    $query = array();
                    $query[] = array(
                        "field" => array(
                            "field" => $attr['contact_id_field_key']
                        ),
                        "op" => "=",
                        "value" => array(
                            "value" => $contact_id
                        )
                    );
                    $condition = json_encode($query);
                    $shipments = $op->searchObject($attr['object_id'], $condition, '', array('sort' => $attr['sort_by'], 'sorDir' => $attr['sort']));
                    $shipment_meta = $op->getObjectFields($attr['object_id']);
                    //print_r($shipment_meta);
                    if (!empty($attr['fields_key']) && is_array($shipments) && count( $shipments ) > 0) {
                        $fields_key = explode(",", $attr['fields_key']);
                        foreach ($shipments as $shipment) {
                            ?>
                            <tr>
                                <?php
                                $cnt = 0;
                                foreach ($fields_key as $key) {
                                    $key = trim($key);
                                    $cnt++;
                                    $value = "";
                                    $key = trim($key);
                                    $extra_class = '';
                                    if ($key == "sn") {
                                        $value = $cnt;
                                    } else {
                                        if (isset($shipment_meta->$key) && isset($shipment->$key)) {

                                            $value = $shipment->$key;
                                            if ($shipment_meta->$key->type == 'fulldate' || $shipment_meta->$key->type == 'timestamp') {
                                                $value = !empty($value) ? date_i18n(get_option('date_format'), $value) : "--";
                                            }
                                            if ($shipment_meta->$key->type == 'drop') {
                                                $extra_class = ' op_field_key_value_' . $value;
                                                $value = isset($shipment_meta->$key->options->$value) ? $shipment_meta->$key->options->$value : '';

                                            }
                                        }
                                    }
                                    ?>
                                    <?php echo $cnt==1&&$is_edit?('<td><span class="dashicons dashicons-edit ontraninja-edit-btn" data-record=\''.json_encode($shipment).'\'></span></td>'):'';?>
                                    <td data-label="<?php echo $fields_label[$cnt-1]?>" class="op_field_key_<?php echo sanitize_title($key) . $extra_class ?>"><?php echo $value ?></td>
                                    <?php
                                }
                                ?>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="6" class="alert alert-danger">No Records Found</td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <?php
            if( $is_edit || $is_add ){
                $GLOBALS['wct_is_add'] = $is_add;
                $GLOBALS['wct_is_edit'] = $is_edit;
                $GLOBALS['wct_add_form_id'] = $is_add?$attr["add_form_id"]:"";
                $GLOBALS['wct_edit_form_id'] = $is_edit?$attr["edit_form_id"]:"";
                $GLOBALS['wct_email'] = $op->getContactByID($contact_id)->email;
                add_action('wp_footer', function(){
                    ?>
                    <div class="wct-popup">
                        <div class="wct-popup-bg"></div>
                        <div class="wct-popup-content">
                            <div class="wct-popup-close"><span class="dashicons dashicons-no"></span></div>
                            <div class="wct-popup-main-content">
                                <?php
                                if($GLOBALS['wct_is_add']){
                                    ?>
                                    <div class="wct-form-add">
                                        <?php
                                        $form = get_transient('wct_form_'.$GLOBALS["wct_add_form_id"]);
                                        if(empty($form)) {
                                            $form = $op->Request('form', array("id" => $GLOBALS["wct_add_form_id"]));
                                            set_transient('wct_form_'.$GLOBALS["wct_add_form_id"], $form);
                                        }
                                        echo $form;
                                        ?>
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php
                                if($GLOBALS['wct_is_edit']){
                                    ?>
                                    <div class="wct-form-edit">
                                        <?php
                                        $form = get_transient('wct_form_'.$GLOBALS["wct_edit_form_id"]);
                                        if(empty($form)) {
                                            $form = $op->Request('form', array("id" => $GLOBALS["wct_edit_form_id"]));
                                            set_transient('wct_form_'.$GLOBALS["wct_edit_form_id"], $form);
                                        }
                                        echo $form;
                                        ?>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <script>
                        (function($){
                            $(".ontraninja-add-new-btn").click(function(e){
                                e.preventDefault();
                                $(".wct-form-add").show();
                                $(".wct-form-edit").hide();
                                $(".wct-form-add").find('input[type="email"]').val('<?php
                                    echo $GLOBALS["wct_email"]
                                    ?>');
                                $(".wct-form-add").find('input[type="email"]').parents(".moonray-form-element-wrapper").hide();
                                $('.wct-popup').fadeIn();
                            });
                            $(".ontraninja-edit-btn").click(function(e){
                                e.preventDefault();
                                $(".wct-form-add").hide();
                                let record = $(this).data("record");
                                $(".wct-form-edit").find('input[type=text],input[type=email],select,textarea').each(function(){
                                    $(this).val(record[$(this).attr("name")]);
                                });
                                $(".wct-form-edit").find('input[name="unique_id"]').val();
                                $(".wct-form-edit").find('input[name="unique_id"]').parents(".moonray-form-element-wrapper").hide();
                                $(".wct-form-edit").show();
                                $('.wct-popup').fadeIn();
                            });
                            $(".wct-popup-close, .wct-popup-bg").click(function(){
                                $('.wct-popup').fadeOut();
                            });
                            $("form").submit(function(){
                                $('.wct-popup').fadeOut();
                            });
                        })(jQuery);
                    </script>
                    <?php
                }, 1000);
            }
            ?>
            <style>
                .wct-popup {
                    position: fixed;
                    width: 100%;
                    height: 100%;
                    overflow: auto;
                    left: 0;
                    top: 0;
                    z-index: 99999;
                    display: none;
                }

                .wct-popup-bg {
                    width: 100%;
                    height: 100%;
                    position: absolute;
                    background-color: rgba(0,0,0,0.5);
                    cursor: pointer;
                }

                .wct-popup-content {
                    position: absolute;
                    background-color: #fff;
                    max-width: 100%;
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%,-50%);
                    padding: 30px 30px 20px 20px;
                }

                .wct-popup-close {
                    position: absolute;
                    cursor: pointer;
                    right: 0;
                    top: 0;
                    width: 30px;
                    height: 30px;
                    text-align: center;
                    padding-top: 5px;
                }
                .table-responsive {
                    width: 100%;
                    overflow: auto;
                }

                .table-responsive table {
                    min-width: 100%;
                }
                @media (max-width: 768px) {
                    .table-responsive thead {
                        display: none;
                    }

                    .table-responsive td {
                        display: block;
                    }

                    .table-responsive td:before {
                        width: 30%;
                        content: attr(data-label);
                        display: block;
                        float: left;
                    }
                }
            </style>
            <?php
            $content = ob_get_contents();
            ob_end_clean();
            return $content;
        }
    }
}
$ontraninja_cot = new ontraninja_cot();
